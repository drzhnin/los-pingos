## Los Pingos
### Dependencies
* Ruby 3.1.3
* Redis
* ClickHouse

### Run local
Build project

```
docker-compose build
```
### Create DB

#### For tests

```
dcr app bundle exec rake db:create RACK_ENV=test
```
#### For development
```
dcr app bundle exec rake db:create
```

### Run app

```
dcoker-compose up app -d
```
### Run tests

```
docker-compose run --rm test
```

### API Doc

add host: `POST /api/v1/hosts`

Example:
  ```
  curl -X POST "http://localhost:9292/api/v1/hosts" \
  -H "Content-Type: application/json" \
  -d '{"ip":"192.168.1.1"}'
  ```

remove host: `DELETE /api/v1/hosts`

Example:
```
curl -X DELETE "http://localhost:9292/api/v1/hosts?ip=192.168.1.1"
```

get host stats: `GET /api/v1/hosts/stats`

Example:
```
curl -X GET "http://localhost:9292/api/v1/hosts/stats?ip=192.168.1.1&started_at=2022-12-19+00:00:00&ended_at=2022-12-19+23:59:59"
```