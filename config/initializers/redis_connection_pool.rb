# frozen_string_literal: true

class RedisConnectionPool
  DEFAULT_URL = "redis://redis:6379"

  cattr_accessor :fake

  class << self
    def connections
      connections = if fake
                      require "mock_redis"
                      MockRedis.new
                    else
                      ConnectionPool.new(size: 30, timeout: 5) do
                        Redis.new(url: ENV.fetch("REDIS_URL", DEFAULT_URL), db: 1)
                      end
                    end
      @connections ||= connections
    end
  end
end
