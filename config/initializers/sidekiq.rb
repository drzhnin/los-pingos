# frozen_string_literal: true

DEFAULT_REDIS_URL = "redis://redis:6379"

redis = { url: ENV.fetch("REDIS_URL", DEFAULT_REDIS_URL), db: 0 }

Sidekiq.configure_client do |config|
  config.redis = redis
end

Sidekiq.configure_server do |config|
  config.redis = redis
end
