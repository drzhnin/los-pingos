# frozen_string_literal: true

class ClickHouseConnectionPool
  class << self
    def connections
      connections = ConnectionPool.new(size: 2) do
        ClickHouse::Connection.new(ClickHouse.config)
      end
      @connections ||= connections
    end
  end
end
