# frozen_string_literal: true

$LOAD_PATH.unshift(File.join(File.dirname(__FILE__), "..", "app"))
$LOAD_PATH.unshift(File.dirname(__FILE__))

require "rubygems"
require "bundler/setup"
require "dotenv"

Dotenv.load(".env", ".env.#{ENV.fetch('RACK_ENV', nil)}")

Bundler.require :default, ENV.fetch("RACK_ENV", nil)

Dir[File.expand_path("../config/initializers/*.rb", __dir__)].each { |f| require f }
Dir[File.expand_path("../api/**/*.rb", __dir__)].each { |f| require f }
Dir[File.expand_path("../app/**/*.rb", __dir__)].each { |f| require f }
