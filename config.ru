# frozen_string_literal: true

require File.expand_path("config/application", __dir__)

run LosPingos::API
