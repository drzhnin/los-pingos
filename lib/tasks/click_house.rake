# frozen_string_literal: true

namespace :db do
  task :create do
    ClickHouseConnectionPool.connections.with do |connection|
      connection.create_database(ClickHouse.config.database, if_not_exists: true)
      sql = <<-SQL
      CREATE TABLE IF NOT EXISTS ping_data (
        host_ip IPv4,
        event_type LowCardinality(String),
        latency Nullable(Decimal(8, 6)),
        error LowCardinality(String),
        created_at DateTime
      ) ENGINE = MergeTree()
      ORDER BY (host_ip, created_at)
      SQL
      connection.execute(sql)
    end
    puts "Database #{ClickHouse.config.database} created"
  end

  task :drop do
    ClickHouseConnectionPool.connections.with do |connection|
      connection.drop_database(ClickHouse.config.database, if_exists: true)
    end
    puts "Database #{ClickHouse.config.database} dropped"
  end
end
