# frozen_string_literal: true

module LosPingos
  class HostsAPI < Grape::API
    resource :hosts do
      desc "Add host"
      params do
        requires :ip, type: String, desc: "Host IP"
      end
      post do
        result = Hosts::Create.new.call(ip: params[:ip])
        if result.success?
          { status: :success, data: "Host #{result.value!} added" }
        else
          error!({ status: :error, data: result.failure[:errors] }, 422)
        end
      end

      desc "Remove host"
      params do
        requires :ip, type: String, desc: "Host IP"
      end
      delete do
        result = Hosts::Destroy.new.call(ip: params[:ip])
        if result.success?
          { status: :success, data: "Host #{result.value!} removed" }
        else
          error!({ status: :error, data: result.failure[:errors] }, 422)
        end
      end

      desc "Show host stats"
      params do
        requires :ip, type: String, desc: "Host IP"
        requires :started_at, type: DateTime, desc: "Started at"
        requires :ended_at, type: DateTime, desc: "Ended at"
      end
      get :stats do
        result = Hosts::Stats.new.call(ip: params[:ip], started_at: params[:started_at], ended_at: params[:ended_at])
        if result.success?
          { status: :success, data: result.success }
        else
          error!({ status: :error, data: result.failure[:errors] }, 422)
        end
      end
    end
  end
end
