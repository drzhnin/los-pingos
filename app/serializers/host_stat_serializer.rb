# frozen_string_literal: true

class HostStatSerializer
  def call(data)
    {
      avg_rtt: data["avg_rtt"].to_f,
      min_rtt: data["min_rtt"].to_f,
      max_rtt: data["max_rtt"].to_f,
      median_rtt: data["median_rtt"].to_f,
      std_dev: data["std_dev"].to_f,
      loss: (100.0 * data["failed"] / (data["failed"] + data["succeed"])).to_f
    }
  end
end
