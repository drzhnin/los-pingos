# frozen_string_literal: true

module Hosts
  class PingWorker < BaseWorker
    def perform(ip)
      Hosts::Ping.new.call(ip: ip)
    end
  end
end
