# frozen_string_literal: true

module Hosts
  class CheckHostsWorker < BaseWorker
    DEFAULT_BATCH_SIZE = 1_000

    def perform
      HostsStorage.each do |batch|
        batch.each_slice(DEFAULT_BATCH_SIZE) do |hosts|
          args = hosts.map { |ip| [ip] }
          Hosts::PingWorker.perform_bulk(args)
        end
      end
    end
  end
end
