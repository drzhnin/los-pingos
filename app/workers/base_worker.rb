# frozen_string_literal: true

class BaseWorker
  include Sidekiq::Job

  sidekiq_options retry: false
end
