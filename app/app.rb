# frozen_string_literal: true

module LosPingos
  class API < Grape::API
    version "v1", using: :path
    prefix :api
    format :json

    rescue_from Grape::Exceptions::ValidationErrors do |e|
      error!({ status: :error, data: e.message }, 422)
    end

    mount LosPingos::HostsAPI
  end
end
