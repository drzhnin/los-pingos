# frozen_string_literal: true

module Hosts
  class Create
    include Dry::Monads[:result]

    def call(params)
      result = NewHostContract.new.call(ip: params[:ip])
      return Failure(errors: result.errors.to_h) if result.failure?

      host_ip = result.to_h[:ip]
      HostsStorage.add(host_ip)
      Success(host_ip)
    end
  end
end
