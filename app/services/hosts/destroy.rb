# frozen_string_literal: true

module Hosts
  class Destroy
    include Dry::Monads[:result]

    def call(params)
      result = DestroyHostContract.new.call(ip: params[:ip])
      return Failure(errors: result.errors.to_h) if result.failure?

      host_ip = result.to_h[:ip]
      HostsStorage.remove(host_ip)
      Success(host_ip)
    end
  end
end
