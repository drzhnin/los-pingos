# frozen_string_literal: true

module Hosts
  class Error < StandardError; end

  class Ping
    DEFAULT_PING_TIMEOUT = 5

    def call(ip:)
      client = Net::Ping::ICMP.new(ip, nil, ENV.fetch("PING_TIMEOUT", DEFAULT_PING_TIMEOUT))
      raise Error, client.exception.message unless client.ping?

      DataStorage.insert_data(host_ip: ip, event_type: :succeed, latency: client.duration)
    rescue Errno::EHOSTUNREACH, Error => e
      DataStorage.insert_data(host_ip: ip, event_type: :failed, error: e.message)
    end
  end
end
