# frozen_string_literal: true

module Hosts
  class Stats
    include Dry::Monads[:result]

    def call(params)
      result = GetStatsHostContract.new.call(
        ip: params[:ip],
        started_at: params[:started_at],
        ended_at: params[:ended_at]
      )
      return Failure(errors: result.errors.to_h) if result.failure?

      data_stats = DataStorage.get_host_stat(result.to_h)

      return Failure(errors: "Stats not found") unless data_stats.present?

      Success(HostStatSerializer.new.call(data_stats))
    end
  end
end
