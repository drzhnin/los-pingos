# frozen_string_literal: true

class GetStatsHostContract < Dry::Validation::Contract
  params do
    required(:ip).filled(:string)
    required(:started_at).filled(:date)
    required(:ended_at).filled(:date)
  end

  rule(:ip) do
    key.failure("mast be an IPv4, like 192.168.1.1") unless value =~ Resolv::IPv4::Regex
    key.failure("host #{value} doesn't exist") unless HostsStorage.exist?(value)
  end
end
