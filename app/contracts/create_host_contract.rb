# frozen_string_literal: true

class NewHostContract < Dry::Validation::Contract
  params do
    required(:ip).filled(:string)
  end

  rule(:ip) do
    key.failure("mast be an IPv4, like 192.168.1.1") unless value =~ Resolv::IPv4::Regex
    key.failure("host #{value} already added") if HostsStorage.exist?(value)
  end
end
