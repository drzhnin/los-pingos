# frozen_string_literal: true

module DataStorage
  DEFAULT_TABLE = "ping_data"
  DT_FORMAT = "%Y-%m-%d %H:%M:%S"

  class Error < StandardError; end

  class << self
    def insert_data(data)
      connections.with do |connection|
        connection.insert(DEFAULT_TABLE, values: [data.merge(created_at: Time.now.utc.to_i)])
      rescue ClickHouse::DbException => e
        raise Error, e.message
      end
    end

    def get_host_stat(params) # rubocop:disable Metrics/MethodLength
      ip = params.fetch(:ip)
      started_at = params.fetch(:started_at)
      ended_at = params.fetch(:ended_at)
      sql = <<-SQL
        SELECT
          min(latency) as min_rtt,
          max(latency) as max_rtt,
          avg(latency) as avg_rtt,
          quantileExact(latency) as median_rtt,
          stddevSamp(latency) as std_dev,
          countIf(event_type = 'succeed') as succeed,
          countIf(event_type = 'failed') as failed
        FROM #{DEFAULT_TABLE}
        WHERE host_ip = toIPv4('#{ip}')
          AND created_at BETWEEN '#{started_at.strftime(DT_FORMAT)}' AND '#{ended_at.strftime(DT_FORMAT)}'
        FORMAT JSON
      SQL

      connections.with do |connection|
        data = connection.select_one(sql)
        return nil if (data["succeed"] + data["failed"]).zero?

        data
      rescue ClickHouse::DbException => e
        raise Error, e.message
      end
    end

    def clear_table!(table: DEFAULT_TABLE)
      connections.with do |connection|
        connection.truncate_table(table)
      end
    end

    private

    def connections
      ClickHouseConnectionPool.connections
    end
  end
end
