# frozen_string_literal: true

module HostsStorage
  KEY = "hosts"

  class << self
    def add(host)
      redis.with do |connection|
        connection.sadd(KEY, host)
      end
    end

    def remove(host)
      redis.with do |connection|
        connection.srem(KEY, host)
      end
    end

    def exist?(ip)
      redis.with do |connection|
        connection.sismember(KEY, ip)
      end
    end

    def each(&block) # rubocop:disable Metrics/MethodLength
      cursor = 0
      i = 0
      loop do
        cursor, batch = redis.with do |connection|
          connection.sscan(KEY, cursor, count: 10_000)
        end
        break if batch.empty?

        block.call batch
        i += 1
        break if cursor == "0" || i >= 100
      end
    end

    private

    def redis
      RedisConnectionPool.connections
    end
  end
end
