# frozen_string_literal: true

describe Hosts::CheckHostsWorker do
  subject(:check_hosts_jobs) do
    described_class.perform_async
    described_class.drain
  end

  context "without hosts" do
    it "does not call ping worker" do
      expect { check_hosts_jobs }.not_to change(Hosts::PingWorker.jobs, :size)
    end
  end

  context "with hosts" do
    before do
      %w[8.8.8.8 192.168.1.1 192.168.1.2].each { |ip| HostsStorage.add(ip) }
    end

    it "call ping worker for hosts" do
      expect { check_hosts_jobs }.to change(Hosts::PingWorker.jobs, :size).from(0).to(3)
    end
  end
end
