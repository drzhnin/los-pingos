# frozen_string_literal: true

describe Hosts::PingWorker do
  let(:service) { instance_double(Hosts::Ping) }

  it "call ping service for host" do
    allow(service).to receive(:call)
    allow(Hosts::Ping).to receive(:new).and_return(service)
    described_class.perform_async("192.168.1.1")
    described_class.drain

    expect(service).to have_received(:call).with(ip: "192.168.1.1")
  end
end
