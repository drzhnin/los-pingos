# frozen_string_literal: true

module Helpers
  include Rack::Test::Methods

  def app
    LosPingos::API
  end

  def response_status
    last_response.status
  end

  def json_body
    JSON.parse(last_response.body)
  end
end
