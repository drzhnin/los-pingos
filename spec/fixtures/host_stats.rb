# frozen_string_literal: true

module HostStats
  extend ActiveSupport::Concern

  def create_stats(ip:, failed: 0, latencies: [])
    latencies.each { |latency| DataStorage.insert_data(host_ip: ip, event_type: :succeed, latency: latency) }
    failed.times { DataStorage.insert_data(host_ip: ip, event_type: :failed, error: "some error") }
  end
end
