# frozen_string_literal: true

describe "Remove host", type: :request do
  subject(:destroy_host) { delete "/api/v1/hosts", { ip: ip } }

  let(:ip) { "192.168.1.1" }

  context "when host already exist" do
    before { HostsStorage.add(ip) }

    it "responds with 201" do
      destroy_host
      expect(response_status).to eq(200)
    end

    it "render valid json" do
      destroy_host
      expect(json_body).to eq({ "data" => "Host 192.168.1.1 removed", "status" => "success" })
    end
  end

  context "when host doesn't exist" do
    it "responds with 201" do
      destroy_host
      expect(response_status).to eq(422)
    end

    it "render valid json" do
      destroy_host
      expect(json_body).to eq({ "data" => { "ip" => ["host 192.168.1.1 doesn't exist"] }, "status" => "error" })
    end
  end
end
