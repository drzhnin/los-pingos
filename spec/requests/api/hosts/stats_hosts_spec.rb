# frozen_string_literal: true

describe "Stats host", type: :request do
  include HostStats

  subject(:get_stats) { get route }

  let(:route) { "/api/v1/hosts/stats/?#{params.to_query}" }
  let(:current_time) { Time.parse("2022-12-19 14:00:00") }

  before do
    allow(Time).to receive(:now).and_return(current_time)
  end

  context "with valid params" do
    let(:ip) { "192.168.1.1" }
    let(:params) do
      {
        ip: ip,
        started_at: (Time.now.utc - 3600).to_s,
        ended_at: Time.now.utc.to_s
      }
    end

    let(:expected_result) do
      {
        "data" => {
          "avg_rtt" => 0.2,
          "loss" => 33.333333333333336,
          "max_rtt" => 0.3,
          "median_rtt" => 0.2,
          "min_rtt" => 0.1,
          "std_dev" => 0.08164965808869012
        },
        "status" => "success"
      }
    end

    before do
      HostsStorage.add(ip)
      create_stats(ip: ip, failed: 2, latencies: [0.1, 0.2, 0.3, 0.2])
    end

    it "render with 200 status" do
      get_stats
      expect(response_status).to eq(200)
    end

    it "render valid json" do
      get_stats
      expect(json_body).to eq(expected_result)
    end
  end

  context "with invalid params" do
    let(:params) do
      {
        ip: "111.123.13333",
        started_at: "invalid_date_time",
        ended_at: "2022-12-18 20:00:00"
      }
    end

    it "responds with 422" do
      get_stats
      expect(response_status).to eq(422)
    end

    it "renders correct json body with error" do
      get_stats
      expect(json_body).to eq({ "data" => "started_at is invalid", "status" => "error" })
    end
  end

  context "when stats dont present" do
    let(:ip) { "192.168.1.1" }
    let(:params) do
      {
        ip: ip,
        started_at: (Time.now.utc - 3600).to_s,
        ended_at: Time.now.utc.to_s
      }
    end

    before { HostsStorage.add(ip) }

    it "responds with 422" do
      get_stats
      expect(response_status).to eq(422)
    end

    it "renders correct json body with error" do
      get_stats
      expect(json_body).to eq({ "data" => "Stats not found", "status" => "error" })
    end
  end
end
