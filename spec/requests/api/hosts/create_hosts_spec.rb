# frozen_string_literal: true

describe "Create host", type: :request do
  subject(:add_host) { post "/api/v1/hosts", { ip: ip } }

  context "with valid ip" do
    let(:ip) { "192.168.1.1" }

    it "responds with 201" do
      add_host
      expect(response_status).to eq(201)
    end

    it "render valid json" do
      add_host
      expect(json_body).to eq({ "data" => "Host 192.168.1.1 added", "status" => "success" })
    end

    context "when host already added" do
      before { HostsStorage.add(ip) }

      it "responds with 201" do
        add_host
        expect(response_status).to eq(422)
      end

      it "render json with error" do
        add_host
        expect(json_body).to eq({ "data" => { "ip" => ["host 192.168.1.1 already added"] }, "status" => "error" })
      end
    end
  end

  context "with invalid ip" do
    let(:ip) { "192.168.1111.1111" }

    it "responds with 201" do
      add_host
      expect(response_status).to eq(422)
    end

    it "render json with error" do
      add_host
      expect(json_body).to eq({ "data" => { "ip" => ["mast be an IPv4, like 192.168.1.1"] }, "status" => "error" })
    end
  end
end
