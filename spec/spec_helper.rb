# frozen_string_literal: true

ENV["RACK_ENV"] = "test"

require "simplecov"

SimpleCov.minimum_coverage 90

SimpleCov.profiles.define "custom_profile" do
  add_filter "/config/"
  add_filter "/spec/"
end
SimpleCov.start

require "sidekiq/testing"
require File.expand_path("../config/application", __dir__)
Dir[File.expand_path("support/**/*.rb", __dir__)].each { |f| require f }
Dir[File.expand_path("fixtures/**/*.rb", __dir__)].each { |f| require f }
RedisConnectionPool.fake = true
Sidekiq::Testing.fake!

RSpec.configure do |config|
  config.expect_with :rspec do |expectations|
    expectations.include_chain_clauses_in_custom_matcher_descriptions = true
  end

  config.mock_with :rspec do |mocks|
    mocks.verify_partial_doubles = true
  end

  config.shared_context_metadata_behavior = :apply_to_host_groups
  config.order = :random

  config.include Helpers, type: :request

  config.before do
    RedisConnectionPool.connections.with(&:flushdb)
    DataStorage.clear_table!
    Sidekiq::Worker.clear_all
  end
end
